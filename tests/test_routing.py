import json
import pytest

from lowlight.http import JSONRequest
from lowlight.responses import redirect

from tests.utils import json_encode, method_not_allowed


def test_url_param(test_app):
    @test_app.route('/<int:param>')
    def view(param):
        return {'param': param}

    client = test_app.test_client()
    response = client.get('/42')

    assert response.data == json_encode({'param': 42})

def test_url_param_default(test_app):
    @test_app.route('/')
    @test_app.route('/<int:param>')
    def view(param=42):
        return {'param': param}

    client = test_app.test_client()

    response = client.get('/')
    assert response.data == json_encode({'param': 42})

    response = client.get('/99')
    assert response.data == json_encode({'param': 99})

def test_url_param_and_injection(test_app):
    @test_app.route('/<int:url_param>')
    def view(url_param, request: JSONRequest):
        return {
            'url_param': url_param,
            'method': request.method,
            'json': request.json,
        }
    
    client = test_app.test_client()
    response = client.post('/42', data=json.dumps({'a':1}),
                           content_type='application/json')
    
    result = {
        'url_param': 42,
        'method': 'POST',
        'json': {'a': 1},
    }
    assert response.data == json_encode(result)

def test_method_not_allowed(test_app):
    @test_app.route('/', methods=['POST'])
    def view():
        return {}

    response = test_app.test_client().get('/')
    assert response.headers['Content-Type'] == 'application/json'
    assert response.data == method_not_allowed()

def test_url_for(test_app):
    @test_app.route('/')
    def view():
        return {}

    assert test_app.url_for('view') == '/'

def test_url_for_params(test_app):
    @test_app.route('/api')
    @test_app.route('/api/<int:id>')
    def view(id):
        return {'id': id}

    assert test_app.url_for('view') == '/api'
    assert test_app.url_for('view', id=42) == '/api/42'
    assert test_app.url_for('view', id=42, non_required=1) == '/api/42?non_required=1'

def test_redirect(test_app):
    @test_app.route('/user')
    def home():
        return {}

    @test_app.route('/login')
    def login():
        url = test_app.url_for('home')
        return redirect(url)

    response = test_app.test_client().get('/login')

    assert response.status_code == 301
    assert response.headers.get('Location') == 'http://localhost/user'
