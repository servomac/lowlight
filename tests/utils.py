import json

def json_encode(data):
    # TODO do not hardcode the dumps parameters, use a framework jsonify
    return json.dumps(data, sort_keys=True, indent=2).encode()

def bad_request(error_msg):
    data = {
        "name": "Bad Request",
        "code": 400,
        "description": error_msg,
    }
    return json.dumps(data).encode()

def method_not_allowed():
    data = {
        "name": "Method Not Allowed",
        "code": 405,
        "description": "The method is not allowed for the requested URL.",
    }
    return json.dumps(data).encode()
