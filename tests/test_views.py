from tests.utils import json_encode

def test_text_response(test_app):
    @test_app.route('/')
    def test_view():
        return 'Hello World'

    response = test_app.test_client().get('/')
    assert response.data == b'Hello World'
    assert response.status_code == 200

def test_status_code(test_app):
    @test_app.route('/')
    def test_view():
        return {}, 201

    response = test_app.test_client().get('/')
    assert response.data == json_encode({})
    assert response.status_code == 201

def test_json_response(test_app):
    @test_app.route('/')
    def test_view():
        return {}

    response = test_app.test_client().get('/')
    assert response.headers['Content-Type'] == 'application/json'
    assert response.data == json_encode({})
    assert response.status_code == 200
