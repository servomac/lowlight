import pytest

from lowlight import App

@pytest.fixture
def test_app():
    return App()
