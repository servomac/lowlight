import json
import pytest

from tests.utils import json_encode, bad_request

@pytest.fixture
def simple_client(test_app):
    @test_app.route('/')
    def test_view(msg):
        return {'msg': msg}

    return test_app.test_client()

def test_param_injection(simple_client):
    response = simple_client.post('/', data=json.dumps({'msg': 'injected'}),
                           content_type='application/json')
    assert response.data == json_encode({'msg': 'injected'})

def test_param_injection_failure_without_json(simple_client):
    response = simple_client.post('/', data='Some data')
    assert response.status_code == 400

    error_msg = 'The input data should be provided via JSON.'
    assert response.data == bad_request(error_msg)

def test_param_injection_not_provided(simple_client):
    response = simple_client.get('/')
    assert response.status_code == 200
    assert response.data == json_encode({'msg': None})

def test_param_injection_default_value(test_app):
    @test_app.route('/')
    def test_view(msg='default'):
        return {'msg': msg}

    client = test_app.test_client()

    response = client.get('/')
    assert response.data == json_encode({'msg': 'default'})

    response = client.post('/', data=json.dumps({'msg': 'injected'}),
                           content_type='application/json')
    assert response.data == json_encode({'msg': 'injected'})

def test_annotated_param_injection_casting(test_app):
    @test_app.route('/')
    def test_view(id: int = 42):
        return {'id': id}

    client = test_app.test_client()
    response = client.get('/')
    assert response.data == json_encode({'id': 42})

    response = client.post('/', data=json.dumps({'id': 999}),
                           content_type='application/json')
    assert response.data == json_encode({'id': 999})

def test_annotated_param_injection_failure(test_app):
    @test_app.route('/')
    def test_view(msg: int):
        return {'msg': msg}

    client = test_app.test_client()
    response = client.post('/', data=json.dumps({'msg': 'random string'}),
                           content_type='application/json')

    error_msg = "Parameter 'msg' defined as <class 'int'> but has value 'random string'."
    assert response.status_code == 400
    assert response.data == bad_request(error_msg)

def test_annotated_param_injection_request(test_app):
    from lowlight.http import JSONRequest
    @test_app.route('/')
    def test_view(request: JSONRequest):
        return {
            'method': request.method,
            'json': request.json,
        }

    client = test_app.test_client()

    response = client.get('/')
    result = {
        'method': 'GET',
        'json': None,
    }
    assert response.data == json_encode(result)

    response = client.post('/', data=json.dumps({'request': 1, 'test': [1, 2, 3]}),
                           content_type='application/json')

    result = {
        'method': 'POST',
        'json': {
            'request': 1,
            'test': [1, 2, 3],
        },
    }
    assert response.data == json_encode(result)
