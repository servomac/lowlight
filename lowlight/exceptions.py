import json

from werkzeug.exceptions import HTTPException

def get_body(self, environ=None):
    return json.dumps({
        'name':         self.name,
        'code':         self.code,
        'description':  self.description,
    })

def get_headers(self, environ=None):
    return [('Content-Type', 'application/json')]

HTTPException.get_body = get_body
HTTPException.get_headers = get_headers
