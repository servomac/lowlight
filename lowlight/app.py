import json
from inspect import signature, Parameter

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import BadRequest

from .exceptions import HTTPException
from .http import JSONRequest

def _json(data):
    return json.dumps(data, sort_keys=True, indent=2)

class App:

    config = {
        'SERVER_NAME': '127.0.0.1',
    }

    def __init__(self):
        self.view_functions = {}
        self.url_map = Map()

    def add_url_rule(self, rule, view_func, methods=None, endpoint=None, **kwargs):
        if endpoint is None:
            endpoint = view_func.__name__

        if methods is None:
            methods = getattr(view_func, 'methods', ['GET', 'POST'])

        rule = Rule(rule, methods=methods, endpoint=endpoint)
        self.url_map.add(rule)
        self.view_functions[endpoint] = view_func

    def route(self, endpoint, **kwargs):
        def decorator(f):
            self.add_url_rule(endpoint, view_func=f, **kwargs)
            return f
        return decorator

    def url_for(self, endpoint, **kwargs):
        adapter = self.url_map.bind(self.config.get('SERVER_NAME'))
        return adapter.build(endpoint, kwargs)

    @staticmethod
    def _jsonify(rv):
        if not isinstance(rv, (list, tuple, dict)):
            return Response(rv)

        status_code = 200
        if isinstance(rv, (list, tuple)) and len(rv) == 2:
           rv, status_code = rv

        response = Response(_json(rv))
        response.headers['content-type'] = 'application/json'
        response.status_code = status_code
        return response

    @staticmethod
    def _get_injectable_params(view, request: JSONRequest):
        def default(param):
            if param.annotation is JSONRequest:
                return request
            if param.default is Parameter.empty:
                return None
            return param.default

        def is_annotated_param(param, request):
            return param.name in request.json and \
               param.annotation is not Parameter.empty

        def type_casting(param, request):
            try:
                return param.annotation(request.json.get(param.name))
            except Exception:
                error_msg = "Parameter '{}' defined as {} but has value '{}'.".format(
                    param.name,
                    param.annotation,
                    request.json.get(param.name),
                )
                raise BadRequest(error_msg)

        def get_request_parameter(param, request):
            if is_annotated_param(param, request):
                if param.annotation is JSONRequest:
                    return request
                return type_casting(param, request)

            return request.json.get(param.name, default(param))

        if request.data and not request.json:
            raise BadRequest('The input data should be provided via JSON.')

        params = signature(view).parameters.values()
        if not request.json:
            return {p.name: default(p) for p in params}

        return {
            param.name: get_request_parameter(param, request)
            for param in params
        }

    def dispatch_request(self, request: JSONRequest):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            view_func = self.view_functions[endpoint]

            params = self._get_injectable_params(view_func, request)
            rv = view_func(**{**params, **values})
            return self._jsonify(rv)
        except HTTPException as e:
            return e

    def wsgi_app(self, environ, start_response):
        request = JSONRequest(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def run(self, host=None, port=8080):
        if host is None:
            host = self.config.get('SERVER_NAME')

        from werkzeug.serving import run_simple
        run_simple(host, port, self)

    def test_client(self):
        from werkzeug.test import Client
        from werkzeug.wrappers import BaseResponse

        return Client(self, BaseResponse)
