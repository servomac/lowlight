from werkzeug.routing import RequestRedirect

def redirect(url):
    raise RequestRedirect(url)
