from json import loads

from werkzeug.utils import cached_property
from werkzeug.wrappers import Request

class JSONRequest(Request):
    # accept up to 4MB of transmitted data.
    max_content_length = 1024 * 1024 * 4

    @cached_property
    def json(self):
        if self.headers.get('content-type') == 'application/json':
            return loads(self.data.decode('utf-8'))
