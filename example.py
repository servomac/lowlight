from lowlight import App

app = App()

@app.route('/', methods=['OPTIONS'])
def main(param: int):
    if param is None:
        param = 42
    return {'param': param}

if __name__ == '__main__':
    app.run()
