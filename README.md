# Lowlight

*Just for fun and learning!*

[![build status](https://gitlab.com/servomac/lowlight/badges/master/build.svg)](https://gitlab.com/servomac/lowlight/commits/master) [![coverage report](https://gitlab.com/servomac/lowlight/badges/master/coverage.svg)](https://gitlab.com/servomac/lowlight/commits/master)

A highly opinionated Python microframework designed to write simple JSON APIs. Based in [Werkzeug](http://werkzeug.pocoo.org/), and inspired by Flask and APIStar.

Features:
 - Parameter injection
 - Type annotation support

### TODO
 - ~~Inject an entire request~~
 - ~~URLs (route params and url reversal)~~
 - ~~Specify status code of response~~
 - Inject and serialize via schema annotation (as APIStar)

## Quickstart

```python
from lowlight import App

app = App()

@app.route('/')
def say_hello(name: str = 'World'):
    return {'Hello': name}

if __name__ == '__main__':
    app.run()
```

## Writting views

You can directly inject parameters into the views directly from JSON request body. If expected parameters are not present in the request, they are injected with their default parameter (or None):

```python
@app.route('/')
def index(msg):
    return {'msg': msg}
```

You also have the possibility to use type annotation to inject the entire request. `JSONRequest` is just a wrapper around werkzeug request that provides a json method:

```python
from lowlight.http import JSONRequest

@app.route('/')
def index(request: JSONRequest):
    return {
        'method': request.method,
        'json': request.json,
    }
```

### Responses

If the view returns a dictionary, it will return a response with `Content-Type: application/json` and a JSON body. If you raise an HTTPException, also a JSON will be provided.

You can specify the status code of the response returning a tuple from the view:

```python
def view():
    return {}, 201
```

Lowlight also provides a redirection response:

```python
from lowlight.responses import redirect
def view():
    return redirect('/url')
```

## URLs

Lowlight provides a `route` decorator using [Werkzeug URL routing](http://werkzeug.pocoo.org/docs/0.12/routing/).

See an example of URL route parameter passing:

```python
@app.route('/user/')
@app.route('/user/<int:user_id>')
def user_detail(user_id=None):
    if user_id is None:
        return {'error': 'You need to register' }

    return {'msg': f'You claim to be user {user_id}'}
```

You can specify the allowed methods for a view. If not defined, GET and POST are allowed by default.

```python
@app.route('/main', methods=['GET'])
def main():
    return {}
```

Url reversal is provided via `url_for` method of the App instance.

```python
from lowlight.response import redirect

@app.route('/<str:name>')
def user(name):
    return {'hello': name}

@app.route('/')
def main():
    url = app.url_for('/', user='username')
    return redirect(url)
```

## Schemas

 *TODO*
